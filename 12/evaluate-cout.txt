INFO - intensifiers - Running command 'evaluate'
INFO - intensifiers - Started run with ID "12"
INFO - evaluate - Get model
INFO - models.load - Load model [/scratch/ws/0/nphilipp-intensifiers/12/model]
2021-12-07 10:59:13.610029: I tensorflow/stream_executor/platform/default/dso_loader.cc:48] Successfully opened dynamic library libcuda.so.1
2021-12-07 10:59:13.662165: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1716] Found device 0 with properties: 
pciBusID: 0004:06:00.0 name: Tesla V100-SXM2-32GB computeCapability: 7.0
coreClock: 1.53GHz coreCount: 80 deviceMemorySize: 31.50GiB deviceMemoryBandwidth: 836.37GiB/s
2021-12-07 10:59:13.662194: I tensorflow/stream_executor/platform/default/dso_loader.cc:48] Successfully opened dynamic library libcudart.so.10.1
2021-12-07 10:59:13.669450: I tensorflow/stream_executor/platform/default/dso_loader.cc:48] Successfully opened dynamic library libcublas.so.10
2021-12-07 10:59:13.672992: I tensorflow/stream_executor/platform/default/dso_loader.cc:48] Successfully opened dynamic library libcufft.so.10
2021-12-07 10:59:13.675785: I tensorflow/stream_executor/platform/default/dso_loader.cc:48] Successfully opened dynamic library libcurand.so.10
2021-12-07 10:59:13.679937: I tensorflow/stream_executor/platform/default/dso_loader.cc:48] Successfully opened dynamic library libcusolver.so.10
2021-12-07 10:59:13.683863: I tensorflow/stream_executor/platform/default/dso_loader.cc:48] Successfully opened dynamic library libcusparse.so.10
2021-12-07 10:59:13.688080: I tensorflow/stream_executor/platform/default/dso_loader.cc:48] Successfully opened dynamic library libcudnn.so.7
2021-12-07 10:59:13.692387: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1858] Adding visible gpu devices: 0
2021-12-07 10:59:13.695504: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1716] Found device 0 with properties: 
pciBusID: 0004:06:00.0 name: Tesla V100-SXM2-32GB computeCapability: 7.0
coreClock: 1.53GHz coreCount: 80 deviceMemorySize: 31.50GiB deviceMemoryBandwidth: 836.37GiB/s
2021-12-07 10:59:13.695526: I tensorflow/stream_executor/platform/default/dso_loader.cc:48] Successfully opened dynamic library libcudart.so.10.1
2021-12-07 10:59:13.695551: I tensorflow/stream_executor/platform/default/dso_loader.cc:48] Successfully opened dynamic library libcublas.so.10
2021-12-07 10:59:13.695575: I tensorflow/stream_executor/platform/default/dso_loader.cc:48] Successfully opened dynamic library libcufft.so.10
2021-12-07 10:59:13.695598: I tensorflow/stream_executor/platform/default/dso_loader.cc:48] Successfully opened dynamic library libcurand.so.10
2021-12-07 10:59:13.695621: I tensorflow/stream_executor/platform/default/dso_loader.cc:48] Successfully opened dynamic library libcusolver.so.10
2021-12-07 10:59:13.695644: I tensorflow/stream_executor/platform/default/dso_loader.cc:48] Successfully opened dynamic library libcusparse.so.10
2021-12-07 10:59:13.695667: I tensorflow/stream_executor/platform/default/dso_loader.cc:48] Successfully opened dynamic library libcudnn.so.7
2021-12-07 10:59:13.700250: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1858] Adding visible gpu devices: 0
2021-12-07 10:59:13.700273: I tensorflow/stream_executor/platform/default/dso_loader.cc:48] Successfully opened dynamic library libcudart.so.10.1
2021-12-07 10:59:14.201135: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1257] Device interconnect StreamExecutor with strength 1 edge matrix:
2021-12-07 10:59:14.201170: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1263]      0 
2021-12-07 10:59:14.201179: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1276] 0:   N 
2021-12-07 10:59:14.207756: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1402] Created TensorFlow device (/job:localhost/replica:0/task:0/device:GPU:0 with 30151 MB memory) -> physical GPU (device: 0, name: Tesla V100-SXM2-32GB, pci bus id: 0004:06:00.0, compute capability: 7.0)
WARNING:tensorflow:Compiled the loaded model, but the compiled metrics have yet to be built. `model.compile_metrics` will be empty until you train or evaluate the model.
WARNING - tensorflow - Compiled the loaded model, but the compiled metrics have yet to be built. `model.compile_metrics` will be empty until you train or evaluate the model.
INFO - models.log_param_count - Model: functional_1
INFO - models.log_param_count -   Total params: 429,403
INFO - models.log_param_count -   Trainable params: 429,403
INFO - models.log_param_count -   Non-trainable params: 0
INFO - datasets.json.vocab - Load vocab from /projects/p003/p_sprachkultur/nphilipp/deep_learning/datasets/deu_intensifiers/vocab.json.
INFO - datasets.csv.load - Load /projects/p003/p_sprachkultur/nphilipp/deep_learning/datasets/deu_intensifiers/train.csv.
INFO - datasets.csv.load - Load /projects/p003/p_sprachkultur/nphilipp/deep_learning/datasets/deu_intensifiers/val.csv.
INFO - datasets.texts.get - X[input_adjd] length: 1
INFO - datasets.texts.get - X[input_stack] length: 7
INFO - datasets.texts.get - Y[output_next] length: 283
INFO - datasets.texts.get - Train on 85315 samples.
INFO - evaluate - Start prediction
2021-12-07 11:00:00.785466: I tensorflow/stream_executor/platform/default/dso_loader.cc:48] Successfully opened dynamic library libcublas.so.10
2021-12-07 11:00:00.990729: I tensorflow/stream_executor/platform/default/dso_loader.cc:48] Successfully opened dynamic library libcudnn.so.7
INFO - evaluate - Save predictions
INFO - evaluate - Start evaluation
INFO - intensifiers - Result: {'loss': 2.3187241554260254, 'categorical_accuracy': 0.47998592257499695, 'precision': 0.8123365044593811, 'recall': 0.3712477385997772}
INFO - intensifiers - Completed after 0:01:56
