# -*- coding: utf-8 -*-
# vim: ft=python fileencoding=utf-8 sts=4 sw=4 et:
"""Experiment with intensifiers."""

import gzip
import json
import numpy as np
import os

from csv import DictWriter
from ingredients import callbacks, datasets, experiments, models, optimizers, paths
from ingredients.decorators import runtime
from logging import Logger
from sacred import Experiment
from sacred.run import Run
from sacred.utils import apply_backspaces_and_linefeeds
from tensorflow.keras.callbacks import History
from tensorflow.keras.models import Model
from tensorflow.keras.utils import Sequence as TFSequence
from typing import Container, Dict, Optional, Tuple, Union


name = "intensifiers"
ex = Experiment(
    name,
    ingredients=[
        callbacks.ingredient,
        datasets.ingredient,
        experiments.ingredient,
        models.ingredient,
        paths.ingredient,
    ],
)
ex.captured_out_filter = apply_backspaces_and_linefeeds


@ex.config
def config():
    """Experiment config."""
    batch_size = 256
    epochs = 100


@callbacks.ingredient.config
def callbacks_config():
    """Callbacks config."""
    earlystopping = {
        "monitor": "val_loss",
        "patience": 10,
        "mode": "auto",
        "restore_best_weights": True,
    }
    reducelronplateau = {
        "monitor": "val_loss",
        "factor": 0.5,
        "patience": 5,
        "min_lr": 0.0001,
    }
    tensorboard = {}


@datasets.texts.ingredient.config
def texts_config():
    """Dataset config."""
    mode = "separate"
    dataset = "deu_intensifiers/our-list_no-downtoners_underscore"
    x_fieldnames = [
        "tweet",
        "direction",
    ]
    y_fieldnames = ["next"]
    id_fieldname = "id"
    vocab_path = "vocab.json"
    vocab_fieldnames = ["adjd", "stack"]
    to_categorical_fields = {"next": 200}
    x_append_one = ["tweet"]
    y_append_one = False
    sample_weights = False
    dtype = {
        "tweet": np.uint,
        "direction": np.int,
        "next": np.uint,
    }


@models.ingredient.config
def models_config():
    """Model config."""
    blocks = [
        {"t": "rnn", "config": {"nb_layers": 1}, "inputs": "input-0"},
        {"t": "merge", "config": {"t": "multiply"}, "inputs": [-1, "input-1"]},
    ]
    layers = {
        "embedding": {"input_dim": 106, "output_dim": 32, "mask_zero": True},
        "embedding_dropout": {
            "class_name": "SpatialDropout1D",
            "config": {"rate": 0.1},
        },
        "bidirectional": {"merge_mode": "concat"},
        "recurrent": {
            "class_name": "GRU",
            "config": {
                "dropout": 0.1,
                "units": 512,
                "recurrent_dropout": 0.1,
            },
        },
    }
    inputs = [
        {"t": "embedding", "shape": (141,), "name": "tweet"},
        {"t": "input", "shape": (1,), "name": "direction"},
    ]
    outputs = [
        {
            "t": "class",
            "name": "next",
            "nb_classes": 200,
            "activation": "softmax",
            "loss": "categorical_crossentropy",
            "metrics": ["categorical_accuracy", "precision", "recall"],
            "layer": "dense",
        },
    ]


@models.ingredient.named_config
def concatenate():
    """Named model config for concatenate."""
    blocks = [
        {"t": "rnn", "config": {"nb_layers": 1}, "inputs": "input-0"},
        {"t": "merge", "config": {"t": "concatenate"}, "inputs": [-1, "input-1"]},
    ]


@models.ingredient.named_config
def conv1d():
    """Named model config for conv1d."""
    blocks = [
        {"t": "merge", "config": {"t": "multiply"}},
        {"t": "conv1d", "config": {"connection_type": "densely", "nb_layers": 32}},
        {"t": "flatten"},
    ]
    layers = {
        "conv1d": {
            "filters": 4,
            "kernel_size": 3,
            "activation": "tanh",
            "padding": "same",
        }
    }


@optimizers.ingredient.config
def optimizers_config():
    """Optimizer config."""
    class_name = "adam"
    amsgrad = True


@ex.capture
def load(batch_size: int = 32) -> Tuple[TFSequence, TFSequence]:
    """Load."""
    return datasets.texts.get(batch_size=batch_size)


@ex.capture
@runtime
def train(
    model: Model,
    train_gen: TFSequence,
    val_gen: Optional[TFSequence],
    batch_size: int = 32,
    epochs: int = 1,
    verbose: int = 1,
    shuffle: bool = True,
    initial_epoch: int = 0,
    class_weight: Optional[Dict] = None,
    max_queue_size: int = 10,
    sample_weight: Optional[np.ndarray] = None,
    workers: int = 1,
    validation_freq: Union[int, Container] = 1,
    use_multiprocessing: bool = False,
) -> History:
    """Train."""
    return model.fit(
        train_gen,
        validation_data=val_gen,
        callbacks=callbacks.get(),
        epochs=epochs,
        verbose=verbose,
        shuffle=shuffle,
        workers=workers,
        initial_epoch=initial_epoch,
        max_queue_size=max_queue_size,
        validation_freq=validation_freq,
        use_multiprocessing=use_multiprocessing,
    )


@ex.command
def evaluate(
    _log: Logger,
    _run: Run,
    batch_size: int = 32,
    verbose: int = 1,
    sample_weight: Optional[np.ndarray] = None,
    steps: Optional[int] = None,
    max_queue_size: int = 10,
    workers: int = 1,
    use_multiprocessing: bool = False,
):
    """Evaluate data."""
    _log.info("Get model")
    model = models.get()

    data_gen = datasets.texts.get(batch_size=batch_size, single_sequence=True)

    chart_data: Dict = {}

    path = os.path.join(_run.observers[0].dir, "predictions.csv.gz")
    with gzip.open(path, "wt", encoding="utf8") as f:
        fieldnames = ["id"]
        for k in data_gen.y.keys():
            fieldnames.append(f"{k}_predicted")
            fieldnames.append(f"{k}_expected")
            fieldnames.append(k)
        writer = DictWriter(f, fieldnames, dialect="unix")
        writer.writeheader()

        _log.info("Predictions...")
        idx = 0
        for i in range(len(data_gen)):
            if data_gen.sample_weights:
                x, y, _ = data_gen[i]
            else:
                x, y = data_gen[i]
            predictions = model.predict_on_batch(x)

            if len(model.outputs) == 1:
                predictions = [predictions]

            for b in range(len(predictions[0])):
                row = {
                    "id": data_gen.ids[idx],
                }
                for j, k in enumerate(data_gen.y.keys()):
                    row[f"{k}_predicted"] = np.argmax(predictions[j][b])
                    row[f"{k}_expected"] = np.argmax(y[k][b])

                    if k not in chart_data:
                        chart_data[k] = {"expected": {}, "true": {}, "false": {}}
                    if row[f"{k}_expected"] not in chart_data[k]["expected"]:
                        chart_data[k]["expected"][row[f"{k}_expected"]] = 0
                    if row[f"{k}_predicted"] not in chart_data[k]["true"]:
                        chart_data[k]["true"][row[f"{k}_predicted"]] = 0
                        chart_data[k]["false"][row[f"{k}_predicted"]] = 0
                    chart_data[k]["expected"][row[f"{k}_expected"]] += 1
                    chart_data[k][
                        "true"
                        if row[f"{k}_expected"] == row[f"{k}_predicted"]
                        else "false"
                    ][row[f"{k}_predicted"]] += 1

                    row[k] = ",".join(str(w) for w in predictions[j][b])
                writer.writerow(row)
                idx += 1

    for k in data_gen.y.keys():
        chart_data[k]["expected"] = {
            "data": [[x, y] for x, y in chart_data[k]["expected"].items()]
        }
        chart_data[k]["true"] = {
            "data": [[x, y] for x, y in chart_data[k]["true"].items()]
        }
        chart_data[k]["false"] = {
            "data": [[x, y] for x, y in chart_data[k]["false"].items()]
        }

    with open(
        os.path.join(_run.observers[0].dir, "chart_data.json"), "w", encoding="utf8"
    ) as f:
        f.write(json.dumps(chart_data))
        f.write("\n")

    _log.info("Start evaluation")
    return model.evaluate(
        data_gen,
        batch_size=batch_size,
        verbose=verbose,
        sample_weight=sample_weight,
        steps=steps,
        max_queue_size=max_queue_size,
        workers=workers,
        use_multiprocessing=use_multiprocessing,
        return_dict=True,
    )


@ex.automain
def experiment(_log: Logger, _run: Run) -> Dict:
    """Experiment."""
    _log.info("Load data")
    train_gen, val_gen = load()

    _log.info("Build model")
    model = models.get()

    _log.info("Start training")
    history, train_time = train(model, train_gen, val_gen)

    _log.info("Save experiment")
    experiments.save(_run.observers[0].dir, model, history.history)
    results = {k: history.history[k][-1] for k in history.history.keys()}
    results["train_time"] = train_time
    return results
