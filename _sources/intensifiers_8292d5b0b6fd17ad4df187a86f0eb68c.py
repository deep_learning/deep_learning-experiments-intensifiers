# -*- coding: utf-8 -*-
# vim: ft=python fileencoding=utf-8 sts=4 sw=4 et:
"""Experiment with intensifiers."""

import numpy as np
import os

from csv import DictWriter
from ingredients import callbacks, datasets, experiments, models, optimizers, paths
from ingredients.decorators import runtime
from logging import Logger
from sacred import Experiment
from sacred.run import Run
from sacred.utils import apply_backspaces_and_linefeeds
from tensorflow.keras.callbacks import History
from tensorflow.keras.models import Model
from tensorflow.keras.utils import Sequence as TFSequence
from typing import Container, Dict, Optional, Tuple, Union


name = "intensifiers"
ex = Experiment(
    name,
    ingredients=[
        callbacks.ingredient,
        datasets.ingredient,
        experiments.ingredient,
        models.ingredient,
        paths.ingredient,
    ],
)
ex.captured_out_filter = apply_backspaces_and_linefeeds


@ex.config
def config():
    """Experiment config."""
    batch_size = 256
    epochs = 100


@callbacks.ingredient.config
def callbacks_config():
    """Callbacks config."""
    # earlystopping = {
    #     "monitor": "val_loss",
    #     "patience": 10,
    #     "mode": "auto",
    #     "restore_best_weights": False,
    # }
    reducelronplateau = {
        "monitor": "val_loss",
        "factor": 0.5,
        "patience": 5,
        "min_lr": 0.0001,
    }
    tensorboard = {}


@datasets.texts.ingredient.config
def texts_config():
    """Dataset config."""
    mode = "separate"
    dataset = "deu_intensifiers"
    x_fieldnames = [
        "tweet",
    ]
    y_fieldnames = ["next"]
    id_fieldname = "id"
    vocab_path = "vocab.json"
    vocab_fieldnames = ["adjd", "stack"]
    to_categorical_fields = {"next": 6667}
    x_append_one = ["tweet"]
    y_append_one = False
    sample_weights = False
    dtype = {
        "tweet": np.uint,
        "next": np.uint,
    }


@models.ingredient.config
def models_config():
    """Model config."""
    blocks = [
        {"t": "rnn", "config": {"nb_layers": 1}},
    ]
    layers = {
        "embedding": {"input_dim": 105, "output_dim": 32, "mask_zero": True},
        "embedding_dropout": {
            "class_name": "SpatialDropout1D",
            "config": {"rate": 0.1},
        },
        "bidirectional": {"merge_mode": "concat"},
        "recurrent": {
            "class_name": "GRU",
            "config": {
                "dropout": 0.1,
                "units": 512,
                "recurrent_dropout": 0.1,
            },
        },
    }
    inputs = [
        {"t": "embedding", "shape": (None,), "name": "tweet"},
    ]
    outputs = [
        {
            "t": "class",
            "name": "next",
            "nb_classes": 6667,
            "activation": "softmax",
            "loss": "categorical_crossentropy",
            "metrics": ["categorical_accuracy", "precision", "recall"],
            "layer": "dense",
        },
    ]


@optimizers.ingredient.config
def optimizers_config():
    """Optimizer config."""
    class_name = "adam"
    amsgrad = True


@ex.capture
def load(batch_size: int = 32) -> Tuple[TFSequence, TFSequence]:
    """Load."""
    return datasets.texts.get(batch_size=batch_size)


@ex.capture
@runtime
def train(
    model: Model,
    train_gen: TFSequence,
    val_gen: Optional[TFSequence],
    batch_size: int = 32,
    epochs: int = 1,
    verbose: int = 1,
    shuffle: bool = True,
    initial_epoch: int = 0,
    class_weight: Optional[Dict] = None,
    max_queue_size: int = 10,
    sample_weight: Optional[np.ndarray] = None,
    workers: int = 1,
    validation_freq: Union[int, Container] = 1,
    use_multiprocessing: bool = False,
) -> History:
    """Train."""
    return model.fit(
        train_gen,
        validation_data=val_gen,
        callbacks=callbacks.get(),
        epochs=epochs,
        verbose=verbose,
        shuffle=shuffle,
        workers=workers,
        initial_epoch=initial_epoch,
        max_queue_size=max_queue_size,
        validation_freq=validation_freq,
        use_multiprocessing=use_multiprocessing,
    )


@ex.command
def evaluate(
    _log: Logger,
    _run: Run,
    batch_size: int = 32,
    verbose: int = 0,
    sample_weight: Optional[np.ndarray] = None,
    steps: Optional[int] = None,
    max_queue_size: int = 10,
    workers: int = 1,
    use_multiprocessing: bool = False,
):
    """Evaluate data."""
    _log.info("Get model")
    model = models.get()

    data_gen = datasets.texts.get(batch_size=batch_size, single_sequence=True)

    _log.info("Start prediction")
    predictions = model.predict(
        data_gen,
        verbose=verbose,
        steps=steps,
        max_queue_size=max_queue_size,
        workers=workers,
        use_multiprocessing=use_multiprocessing,
    )

    if len(model.outputs) == 1:
        predictions = [predictions]

    _log.info("Save predictions")
    path = os.path.join(_run.observers[0].dir, "predictions.csv")
    with open(path, "w", encoding="utf8") as f:
        fieldnames = (
            ["id"]
            + list(data_gen.x.keys())
            + ["output_predicted", "output_expected"]
            + list(data_gen.y.keys())
        )
        writer = DictWriter(f, fieldnames, dialect="unix")
        writer.writeheader()
        for i in range(data_gen.size):
            row = {
                "id": data_gen.ids[i],
            }
            for j, k in enumerate(data_gen.x.keys()):
                if type(data_gen.x[k][1][i]) == int:
                    row[k] = data_gen.vocab.rget(data_gen.x[k][1][i])
                else:
                    row[k] = ",".join(
                        [data_gen.vocab.rget(w) for w in data_gen.x[k][1][i] if w != 1]
                    )
            for j, k in enumerate(data_gen.y.keys()):
                row["output_predicted"] = np.argmax(predictions[j][i])
                row["output_expected"] = np.argmax(data_gen.y[k][1][i])
                row[k] = ",".join(str(i) for i in predictions[j][i])
            writer.writerow(row)

    _log.info("Start evaluation")
    return model.evaluate(
        data_gen,
        batch_size=batch_size,
        verbose=verbose,
        sample_weight=sample_weight,
        steps=steps,
        max_queue_size=max_queue_size,
        workers=workers,
        use_multiprocessing=use_multiprocessing,
        return_dict=True,
    )


@ex.automain
def experiment(_log: Logger, _run: Run) -> Dict:
    """Experiment."""
    _log.info("Load data")
    train_gen, val_gen = load()

    _log.info("Build model")
    model = models.get()

    _log.info("Start training")
    history, train_time = train(model, train_gen, val_gen)

    _log.info("Save experiment")
    experiments.save(_run.observers[0].dir, model, history.history)
    results = {k: history.history[k][-1] for k in history.history.keys()}
    results["train_time"] = train_time
    return results
