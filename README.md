# Intensifiers

## TensorBoard

Follow the instructions to [install](https://www.tensorflow.org/tensorboard/) it. Afterwards run it with this repo as `logdir`, like:
```
$ tensorboard --logdir PATH/TO/deep_learning-experiments-intensifiers
```

## Experiments
* `python3 hpc_experiment.py -A p_sprachkultur --gres gpu:1 -p ml --mem 20G --base-dir /scratch/ws/0/nphilipp-intensifiers/ -t 10:00:00 00:30:00 --experiment-evaluate intensifiers.py --id 1`
* `python3 hpc_experiment.py -A p_sprachkultur --gres gpu:1 -p ml --mem 20G --base-dir /scratch/ws/0/nphilipp-intensifiers/ -t 10:00:00 00:30:00 --experiment-evaluate intensifiers.py --id 2 models.layers.recurrent.config.units=256`
* `python3 hpc_experiment.py -A p_sprachkultur --gres gpu:1 -p ml --mem 20G --base-dir /scratch/ws/0/nphilipp-intensifiers/ -t 10:00:00 00:30:00 --experiment-evaluate intensifiers.py --id 3 models.layers.embedding.output_dim=64`
* `python3 hpc_experiment.py -A p_sprachkultur --gres gpu:1 -p ml --mem 20G --base-dir /scratch/ws/0/nphilipp-intensifiers/ -t 10:00:00 00:30:00 --experiment-evaluate intensifiers.py --id 4 epochs=250`
* `python3 hpc_experiment.py -A p_sprachkultur --gres gpu:1 -p ml --mem 20G --base-dir /scratch/ws/0/nphilipp-intensifiers/ -t 20:00:00 00:30:00 --experiment-evaluate intensifiers.py --id 5 epochs=1000 intensifiers_blocks2.json`
* `python3 hpc_experiment.py -A p_sprachkultur --gres gpu:1 -p ml --mem 20G --base-dir /scratch/ws/0/nphilipp-intensifiers/ -t 20:00:00 00:30:00 --experiment-evaluate intensifiers.py --id 6 intensifiers_conv1d_32.json`
* `python3 hpc_experiment.py -A p_sprachkultur --gres gpu:1 -p ml --mem 20G --base-dir /scratch/ws/0/nphilipp-intensifiers/ -t 20:00:00 00:30:00 --experiment-evaluate intensifiers.py --id 7 epochs=1000 intensifiers_conv1d_32.json`
* `python3 hpc_experiment.py -A p_sprachkultur --gres gpu:1 -p ml --mem 20G --base-dir /scratch/ws/0/nphilipp-intensifiers/ -t 20:00:00 00:30:00 --experiment-evaluate intensifiers.py --id 8 intensifiers_multiply.json`
* `python3 hpc_experiment.py -A p_sprachkultur --gres gpu:1 -p ml --mem 20G --base-dir /scratch/ws/0/nphilipp-intensifiers/ -t 20:00:00 00:30:00 --experiment-evaluate intensifiers.py --id 9 epochs=1000 intensifiers_multiply.json`
* `python3 hpc_experiment.py -A p_sprachkultur --gres gpu:1 -p ml --mem 20G --base-dir /scratch/ws/0/nphilipp-intensifiers/ -t 25:00:00 00:30:00 --experiment-evaluate intensifiers.py --id 10 epochs=1000 intensifiers_multiply_gru2.json`
* `python3 hpc_experiment.py -A p_sprachkultur --gres gpu:1 -p ml --mem 20G --base-dir /scratch/ws/0/nphilipp-intensifiers/ -t 20:00:00 00:30:00 --experiment-evaluate intensifiers.py --id 11 intensifiers_multiply_conv1d_32.json`
* `python3 hpc_experiment.py -A p_sprachkultur --gres gpu:1 -p ml --mem 20G --base-dir /scratch/ws/0/nphilipp-intensifiers/ -t 25:00:00 00:30:00 --experiment-evaluate intensifiers.py --id 12 epochs=1000 intensifiers_multiply_conv1d_32.json`
* `python3 hpc_experiment.py -A p_sprachkultur --gres gpu:1 -p ml --mem 20G --base-dir /scratch/ws/0/nphilipp-intensifiers/ -t 140:00:00 05:00:00 --experiment-evaluate intensifiers.py --id 13 epochs=10`
* `python3 hpc_experiment.py -A p_sprachkultur --gres gpu:1 -p ml --mem 20G --base-dir /scratch/ws/0/nphilipp-intensifiers/ -t 130:00:00 05:00:00 --experiment-evaluate intensifiers.py --id 14 models.layers.recurrent.config.units=256 epochs=10`
* `python3 hpc_experiment.py -A p_sprachkultur --gres gpu:1 -p ml --mem 20G --base-dir /scratch/ws/0/nphilipp-intensifiers/ -t 140:00:00 05:00:00 --experiment-evaluate intensifiers.py --id 15 intensifiers_filtered.json epochs=35`
* `python3 hpc_experiment.py -A p_sprachkultur --gres gpu:1 -p ml --mem 20G --base-dir /scratch/ws/0/nphilipp-intensifiers/ -t 140:00:00 05:00:00 --experiment-evaluate intensifiers.py --id 16 models.layers.recurrent.config.units=256 intensifiers_filtered.json epochs=35`
* `python3 hpc_experiment.py -A p_sprachkultur --gres gpu:1 -p ml --mem 20G --base-dir /scratch/ws/0/nphilipp-intensifiers/ -t 160:00:00 05:00:00 --experiment-evaluate intensifiers.py --id 17`
